﻿using Xamarin.Forms;

namespace MobileKP
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new Views.MainPage();
        }

        protected override void OnStart()
        {

        }

        protected override void OnSleep()
        {
            CurrentDateTimeOnMachine date = (CurrentDateTimeOnMachine)this.Resources["currentDate"];
            date.StopUpdate();
        }

        protected override void OnResume()
        {
            CurrentDateTimeOnMachine date = (CurrentDateTimeOnMachine)this.Resources["currentDate"];
            date.StartUpdate();
        }
    }
}
