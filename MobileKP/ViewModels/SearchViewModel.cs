﻿using MobileKP.Balance;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace MobileKP.ViewModels
{
    class SearchViewModel : INotifyPropertyChanged
    {
        private DateTime _from;
        private DateTime _to;
        private string _name;

        private List<BalanceEvent> results;
        private BalanceEventDatabase db;
        public SearchViewModel()
        {
            db = (BalanceEventDatabase)App.Current.Resources["database"];
            results = new List<BalanceEvent>();

            _from = DateTime.Now.Date;
            _to = DateTime.Now.Date;
            _name = "";
            UpdateResults();
        }

        public List<BalanceEvent> Results
        {
            get { return results; }
        }

        public DateTime From 
        { 
            get { return _from; }
            set 
            { 
                if (_from == null || !_from.Equals(value))
                {
                    _from = value;
                    UpdateResults();
                }
            } 
        }
        public DateTime To 
        { 
            get { return _to; }
            set
            {
                if(_to == null || !_to.Equals(value))
                {
                    _to = value;
                    UpdateResults();
                }
            }
        }
        public string Name 
        { 
            get { return _name; }
            set
            {
                if(_name == null || _name.CompareTo(value) != 0)
                {
                    _name = value;
                    UpdateResults();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        public async void UpdateResults()
        {
            await db.GetBalanceEventsFiltered(_from, _to, _name).ContinueWith((t) =>
            {
                var res = t.Result;
                results = res;
                OnPropertyChanged("Results");
            });
        }
    }
}
