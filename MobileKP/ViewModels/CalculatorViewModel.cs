﻿using MobileKP.Balance;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace MobileKP.ViewModels
{
    class CalculatorViewModel : INotifyPropertyChanged
    {
        private BalanceEventDatabase db;

        private DateTime _from;
        private DateTime _to;
        private bool _balancefromignore;

        private double _result;

        public CalculatorViewModel()
        {
            db = (BalanceEventDatabase)App.Current.Resources["database"];
        }

        public DateTime From
        {
            get { return _from; }
            set
            {
                if (_from == null || !_from.Equals(value))
                {
                    _from = value;
                    UpdateResults();
                }
            }
        }
        public DateTime To
        {
            get { return _to; }
            set
            {
                if (_to == null || !_to.Equals(value))
                {
                    _to = value;
                    UpdateResults();
                }
            }
        }

        public bool BalanceFromIgnore
        {
            get { return _balancefromignore; }
            set
            {
                if (_balancefromignore != value)
                {
                    _balancefromignore = value;
                    UpdateResults();
                }
            }
        } 

        public double Result
        {
            get { return _result; }
        }

        public async void UpdateResults()
        {
            if (_balancefromignore)
            {
                await db.GetBalanceEventsBefore(_to, "").ContinueWith((t) =>
                {
                    CalcBalance(t.Result);
                });
                return;
            }
            await db.GetBalanceEventsFiltered(_from, _to, "").ContinueWith((t) => CalcBalance(t.Result));

        }

        private void CalcBalance(List<BalanceEvent> evs)
        {
            double balance = 0;
            foreach(var e in evs)
            {
                balance += e.BalanceChange;
            }

            _result = balance;
            OnPropertyChanged("Result");
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
