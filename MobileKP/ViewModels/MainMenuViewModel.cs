﻿using MobileKP.Balance;
using System.Collections.Generic;
using System.ComponentModel;

namespace MobileKP.ViewModels
{
    class MainMenuViewModel : INotifyPropertyChanged
    {
        private BalanceEventDatabase db;

        public MainMenuViewModel()
        {
            db = (BalanceEventDatabase)App.Current.Resources["database"];
            db.PropertyChanged += Db_PropertyChanged;
        }

        private void Db_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.CompareTo("TodayBalance") == 0)
            {
                OnPropertyChanged("TodayBalance");
            } else if (e.PropertyName.CompareTo("TodayBalanceEvents") == 0)
            {
                OnPropertyChanged("TodayEvents");
            }
            
        }

        public double TodayBalance
        {
            get { return db.TodayBalance; }
        }

        public List<BalanceEvent> TodayEvents
        {
            get { return db.TodayBalanceEvents; }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
