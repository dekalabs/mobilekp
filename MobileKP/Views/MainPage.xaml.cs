﻿using MobileKP.Balance;
using System;
using Xamarin.Forms;

namespace MobileKP.Views
{
    public partial class MainPage : ContentPage
    {
        private DateTime lastTimeUpdate;
        public MainPage()
        {
            InitializeComponent();

            lastTimeUpdate = DateTime.Now.Date;
            var dateContr = (CurrentDateTimeOnMachine)App.Current.Resources["currentDate"];
            dateContr.PropertyChanged += DateContr_PropertyChanged;
        }

        private void DateContr_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var dateContr = (CurrentDateTimeOnMachine)sender;
            if(lastTimeUpdate != dateContr.Date.Date)
            {
                var db = (BalanceEventDatabase)App.Current.Resources["database"];
                db.OnInsertedOrDeleted();
                lastTimeUpdate = dateContr.Date.Date;
            }
        }

        private async void OnAddEventButtonClicked(object sender, EventArgs args)
        {
            await Navigation.PushModalAsync(new AddEventPage()); 
        }
        private async void OnDeleteEventButtonClicked(object sender, EventArgs args)
        {
            await Navigation.PushModalAsync(new DeleteEventPage());
        }

        private async void OnSearchButtonClicked(object sender, EventArgs args)
        {
            await Navigation.PushModalAsync(new SearchPage());
        }

        private async void OnCalcButtonClicked(object sender, EventArgs args)
        {
            await Navigation.PushModalAsync(new CalculatorPage());
        }

    }
}
