﻿using MobileKP.Balance;
using MobileKP.ViewModels;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileKP.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DeleteEventPage : ContentPage
    {
        private BalanceEventDatabase db;
        public DeleteEventPage()
        {
            db = (BalanceEventDatabase)App.Current.Resources["database"];
            InitializeComponent();
            var model = (SearchViewModel)this.Resources["searchModel"];
            model.UpdateResults();
        }

        public async void OnDeleteButtonClicked(object sender, EventArgs args)
        {
            var listView = (ListView)FindByName("listView");
            if(listView.SelectedItem == null)
            {
                await DisplayAlert("Ошибка", "Не выбран элемент в списке", "OK");
                return;
            }
            var sel = (BalanceEvent)listView.SelectedItem;
            db.DeleteBalanceEvent(sel);
            _ = Navigation.PopModalAsync();
        }
    }
}