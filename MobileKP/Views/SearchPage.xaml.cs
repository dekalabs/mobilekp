﻿using MobileKP.Balance;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileKP.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchPage : ContentPage
    {
        public SearchPage()
        {
            InitializeComponent();
        }

        public async void OnItemSelected(object sender, EventArgs args)
        {
            var list = (ListView)sender;
            if (list.SelectedItem != null)
            {
                await Navigation.PushModalAsync(new EventViewPage((BalanceEvent)list.SelectedItem));
            }
            
        }
    }
}