﻿using MobileKP.Balance;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileKP.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddEventPage : ContentPage
    {
        public AddEventPage()
        {
            InitializeComponent();
        }

        public async void OnAddClicked(object sender, EventArgs args)
        {
            var ev = (BalanceEvent)Resources["balanceEvent"];
            var picker = (Picker)FindByName("type");
            int type = picker.SelectedIndex;
            if (type == -1)
            {
                await DisplayAlert("Ошибка", "Не задан тип события", "OK");
                return;
            } 
            if (type == 1)
            {
                ev.BalanceChange = -ev.BalanceChange;
            }
            if (ev.Name == null || ev.Name.Trim() == "")
            {
                await DisplayAlert("Ошибка", "Не задано наименование", "OK");
                return;
            }

            ev.Date = ev.Date.Date;

            var db = (BalanceEventDatabase)App.Current.Resources["database"];
            db.AddBalanceEvent(ev);
            _ = Navigation.PopModalAsync();

        }
    }
}