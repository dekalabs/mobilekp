﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileKP.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CalculatorPage : ContentPage
    {
        public CalculatorPage()
        {
            InitializeComponent();
        }
    }
}