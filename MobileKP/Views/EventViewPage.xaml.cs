﻿using MobileKP.Balance;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MobileKP.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EventViewPage : ContentPage
    {
        public EventViewPage(BalanceEvent ev)
        {
            InitializeComponent();
            this.BindingContext = ev;
        }
    }
}