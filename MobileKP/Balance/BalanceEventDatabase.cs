﻿using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Threading.Tasks;

namespace MobileKP.Balance
{
    class BalanceEventDatabase : INotifyPropertyChanged
    {
        private SQLiteAsyncConnection connection;
        private double todayBalance;
        private List<BalanceEvent> todayBalanceEvents;
        public BalanceEventDatabase()
        {

            var path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            path = Path.Combine(path, "balance.db");

            connection = new SQLiteAsyncConnection(path, SQLiteOpenFlags.Create | SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.SharedCache, false);
            connection.CreateTableAsync<BalanceEvent>().Wait();

            UpdateTodayBalance();
            UpdateTodayBalanceEvents();
        }

        public double TodayBalance
        {
            get { return todayBalance; }
        }

        public List<BalanceEvent> TodayBalanceEvents
        {
            get { return todayBalanceEvents; }
        }

        public async void AddBalanceEvent(BalanceEvent ev)
        {
            await connection.InsertAsync(ev).ContinueWith((newID) =>
            {
                ev.ID = newID.Result;
                OnInsertedOrDeleted();
            });
        }
        
        public async void DeleteBalanceEvent(BalanceEvent ev)
        {
            await connection.DeleteAsync(ev).ContinueWith((res) =>
            {
                OnInsertedOrDeleted();
            });
        }

        public async Task<List<BalanceEvent>> GetBalanceEventsFiltered(DateTime from, DateTime to, string titlepart)
        {
            var result = connection.Table<BalanceEvent>();
            if (titlepart != null && titlepart.Trim() != "")
            {
                result = result.Where((e) => e.Name.Contains(titlepart));
            }
            if (from != null)
            {
                result = result.Where((e) => from <= e.Date);
            }
            if(to != null)
            {
                result = result.Where((e) => e.Date <= to);
            }
            return await result.ToListAsync();
        }

        public async Task<List<BalanceEvent>> GetBalanceEventsBefore(DateTime to, string titlepart)
        {
            var result = connection.Table<BalanceEvent>();
            if (titlepart != null && titlepart.Trim() != "")
            {
                result = result.Where((e) => e.Name.Contains(titlepart));
            }
            if (to != null)
            {
                result = result.Where((e) => e.Date <= to);
            }
            return await result.ToListAsync();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        public void OnInsertedOrDeleted()
        {
            UpdateTodayBalance();
            UpdateTodayBalanceEvents();
        }

        private async void UpdateTodayBalance()
        {
            await connection.Table<BalanceEvent>().Where((e) => e.Date <= DateTime.Now.Date).ToListAsync()
                .ContinueWith((evs) => {
                    var events = evs.Result;
                    double balance = 0;
                    foreach (var e in events)
                    {
                        balance += e.BalanceChange;
                    }
                    todayBalance = balance;
                    OnPropertyChanged("TodayBalance");
                });
            
        }

        private async void UpdateTodayBalanceEvents()
        {
            await connection.Table<BalanceEvent>().Where((e) => e.Date == DateTime.Now.Date).ToListAsync()
                .ContinueWith((evs) => {
                    var events = evs.Result;
                    todayBalanceEvents = events;
                    OnPropertyChanged("TodayBalanceEvents");
                });
        }
    }
}
