﻿using SQLite;
using System;
using System.ComponentModel;

namespace MobileKP.Balance
{
    [Table("balance")]
    public class BalanceEvent : INotifyPropertyChanged
    {
        private int id;
        private DateTime date;
        private double balanceChange;
        private string name;
        private string description;

        public BalanceEvent()
        {
            id = 0;
            date = DateTime.Now;
            balanceChange = 0;
            name = "";
            description = "";
        }

        [PrimaryKey, AutoIncrement]
        public int ID
        {
            get { return id; }
            set
            {
                if (ID != value)
                {
                    id = value;
                    OnPropertyChanged("ID");
                }
            }
        }

        [Column("date")]
        public DateTime Date
        {
            get { return date; }
            set
            {
                if (date == null || !date.Equals(value))
                {
                    date = value;
                    OnPropertyChanged("Date");
                }
            }
        }
        [Column("balance")]
        public double BalanceChange
        {
            get { return balanceChange; }
            set
            {
                if (balanceChange != value)
                {
                    balanceChange = value;
                    OnPropertyChanged("BalanceChange");
                }
            }
        }

        [Column("name")]
        public string Name
        {
            get { return name; }
            set
            {
                if (name == null || name.CompareTo(value) != 0)
                {
                    name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        [Column("desc")]
        public string Description
        {
            get { return description; }
            set
            {
                if (description == null || description.CompareTo(value) != 0)
                {
                    description = value;
                    OnPropertyChanged("Description");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
