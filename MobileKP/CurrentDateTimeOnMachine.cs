﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;

namespace MobileKP
{
    class CurrentDateTimeOnMachine : INotifyPropertyChanged
    {
        private DateTime date;
        private bool runLoop;
        private bool timerFinished;
        public DateTime Date
        {
            get { return date; }
        }

        public CurrentDateTimeOnMachine()
        {
            runLoop = false;
            timerFinished = true;
            date = DateTime.Now;

            StartUpdate();
        }

        public void StartUpdate()
        {
            if (timerFinished)
            {
                Device.StartTimer(new TimeSpan(0, 0, 1), () =>
                {
                    date = DateTime.Now;
                    OnPropertyChanged("Date");
                    if (!runLoop) {
                        timerFinished = true;
                    }
                    return runLoop;
                });
            }

            runLoop = true;
        }

        public void StopUpdate()
        {
            runLoop = false;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
