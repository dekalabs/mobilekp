﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace MobileKP.Converters
{
    public class DateTimeConverterOnlyDate : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DateTime dt = (DateTime)value;
            return dt.ToString("dd.MM.yyyy");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string v = (string)value;
            return DateTime.Parse(v);
        }
    }
}
