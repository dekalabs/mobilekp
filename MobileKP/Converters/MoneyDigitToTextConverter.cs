﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace MobileKP.Converters
{
    class MoneyDigitToTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var f = (double)value;
            if (f < 0)
            {
                return "Расход: " + (-f).ToString("F2");
            }
            return "Доход: " + f.ToString("F2");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
