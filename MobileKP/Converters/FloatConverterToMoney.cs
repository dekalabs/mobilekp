﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace MobileKP.Converters
{
    class FloatConverterToMoney : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double v = (double)value;
            return v.ToString("F2");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string v = (string)value;
            double res;
            if (double.TryParse(v, out res))
            {
                return res;
            }
            return 0;
        }
    }
}
